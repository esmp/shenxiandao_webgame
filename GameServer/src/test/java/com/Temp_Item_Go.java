package com;

import com.alibaba.fastjson.JSON;

import java.util.Random;

/**
 * 计算道具->兵种转换
 * User: yunai
 * Date: 13-5-3
 * Time: 下午2:04
 */
public class Temp_Item_Go {

    // 骑兵 步兵 工兵

    public static void main(String[] args) {
        // 道具 - 兵种关系
        int a[][][] = new int[3][3][3];
        a[0][0] = new int[]{15, 7, 8};
        a[0][1] = new int[]{7, 8, 15};
        a[0][2] = new int[]{8, 15, 7};
        a[1][0] = new int[]{12, 6, 12};
        a[1][1] = new int[]{6, 12, 12};
        a[1][2] = new int[]{12, 12, 6};
        a[2][0] = new int[]{1, 18, 1};
        a[2][1] = new int[]{1, 1, 18};
        a[2][2] = new int[]{18, 1, 1};
System.out.println(JSON.toJSONString(a));

        // 道具拥有数量
        int b[][] = new int[3][3];
        int max1 = 10;
        int max2 = 10;
        int max3 = 10;
        Random random = new Random();
        b[0] = new int[]{random.nextInt(max1), random.nextInt(max1), random.nextInt(max1)};
        b[1] = new int[]{random.nextInt(max2), random.nextInt(max2), random.nextInt(max2)};
        b[2] = new int[]{random.nextInt(max3), random.nextInt(max3), random.nextInt(max3)};

        // 计算开始
        int bTotal[] = new int[]{b[0][0] + b[0][1] + b[0][2],
                                 b[1][0] + b[1][1] + b[1][2],
                                 b[2][0] + b[2][1] + b[2][2]};
        int minBIndex = 0;
        int minB = bTotal[0];
        if (bTotal[1] < minB) {
            minBIndex = 1;
            minB = bTotal[1];
        }
        if (bTotal[2] < minB) {
            minBIndex = 2;
            minB = bTotal[2];
        }

        // 计算开始1
        int cloneB[][] = new int[3][3];
        System.arraycopy(b[0], 0, cloneB[0], 0, cloneB.length);
        System.arraycopy(b[1], 0, cloneB[1], 0, cloneB.length);
        System.arraycopy(b[2], 0, cloneB[2], 0, cloneB.length);
        int cloneBTotal[] = new int[3];
        System.arraycopy(bTotal, 0, cloneBTotal, 0, cloneBTotal.length);

System.out.println(JSON.toJSONString(b));
    }

    private static void go(int k, int[][] b, int[] bTotal, int minBIndex) {
//        int use[][] = new int[3][3];
        int get[] = new int[3];


    }
}

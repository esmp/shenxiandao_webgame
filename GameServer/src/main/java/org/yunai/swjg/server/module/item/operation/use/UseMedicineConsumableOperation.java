package org.yunai.swjg.server.module.item.operation.use;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.core.constants.SysMessageConstants;
import org.yunai.swjg.server.core.role.Role;
import org.yunai.swjg.server.core.util.RolePropertyUtils;
import org.yunai.swjg.server.module.item.ItemDef;
import org.yunai.swjg.server.module.item.template.ConsumableItemTemplate;
import org.yunai.swjg.server.module.item.vo.Item;
import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.swjg.server.rpc.message.S_C.S_C_SysMessageReq;

/**
 * 使用丹药操作
 * User: yunai
 * Date: 13-6-7
 * Time: 下午7:09
 */
@Component
public class UseMedicineConsumableOperation extends AbstractItemUseOperation {

    private static final int PROP_STRENGTH = 1;
    private static final int PROP_STUNT = 2;
    private static final int PROP_MAGIC = 3;

    @Override
    public boolean isSuitable(Player player, Item item, int targetId) {
        return item.getType() == ItemDef.Type.MEDICINE_CONSUMABLE;
    }

    /**
     * 验证使用丹是否已经到达无法使用的程度。判断标准为丹药加成是否大于0
     *
     * @param player   玩家信息
     * @param item     道具
     * @param targetId 无用参数
     * @param param    无用参数
     * @return 验证结果
     */
    @Override
    protected boolean canUseImpl(Player player, Item item, int targetId, int param) {
        ConsumableItemTemplate template = (ConsumableItemTemplate) item.getTemplate();
        Role role = player.getRole(targetId);
        int count = getMedicineCount(role, template.getParam1(), template.getParam2());
        int add = RolePropertyUtils.genMedicinePropAdd(template.getParam2(), count + 1);
        if (add <= 0) {
            player.message(new S_C_SysMessageReq(SysMessageConstants.PLAYER_MEDICINE_PROP_FULL));
            return false;
        }
        return true;
    }

    @Override
    protected boolean useImpl(Player player, Item item, int targetId, int param) {
        // 扣除丹药
        item.changeOverlap(item.getOverlap() - 1);
        player.getInventory().sendModifyMessage(item);
        // 增加属性
        ConsumableItemTemplate template = (ConsumableItemTemplate) item.getTemplate();
        Role role = player.getRole(targetId);
        int count = getMedicineCount(role, template.getParam1(), template.getParam2());
        setMedicineCount(role, template.getParam1(), template.getParam2(), count + 1);
        return true;
    }

    /**
     * @param role          玩家/伙伴
     * @param medicineParam 丹药使用参数
     * @param medicineLevel 丹药等级
     * @return 某丹药某属性某等级数量
     */
    private int getMedicineCount(Role role, int medicineParam, int medicineLevel) {
        switch (medicineParam) {
            case PROP_STRENGTH:
                return role.getMedicineStrengthCount(medicineLevel);
            case PROP_STUNT:
                return role.getMedicineStuntCount(medicineLevel);
            case PROP_MAGIC:
                return role.getMedicineMagicCount(medicineLevel);
            default: // 该情况会在模版检查里处理，实际是不会出现该情况的。
                throw new IllegalArgumentException("丹药模版的param1出错，请检查！");
        }
    }

    /**
     * 设置某丹药某属性某等级数量
     *
     * @param role          玩家/伙伴
     * @param medicineParam 丹药使用参数
     * @param medicineLevel 丹药等级
     * @param count         某丹药某属性某等级数量
     */
    private void setMedicineCount(Role role, int medicineParam, int medicineLevel, int count) {
        switch (medicineParam) {
            case PROP_STRENGTH:
                role.changeMedicineStrength(medicineParam, count);
                break;
            case PROP_STUNT:
                role.changeMedicineStunt(medicineParam, count);
                break;
            case PROP_MAGIC:
                role.changeMedicineMagic(medicineParam, count);
                break;
        }
    }
}

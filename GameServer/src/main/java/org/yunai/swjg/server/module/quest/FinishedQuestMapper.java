package org.yunai.swjg.server.module.quest;

import org.yunai.swjg.server.entity.FinishedQuestEntity;
import org.yunai.yfserver.persistence.orm.mybatis.Mapper;
import org.yunai.yfserver.persistence.orm.mybatis.SaveMapper;

import java.util.List;

/**
 * FinishedQuest数据访问接口
 * User: yunai
 * Date: 13-5-9
 * Time: 下午10:47
 */
public interface FinishedQuestMapper extends Mapper, SaveMapper<FinishedQuestEntity> {

    /**
     * 获得一个玩家的完成的任务列表
     *
     * @param playerId 玩家编号
     * @return 玩家完成的任务列表
     */
    List<FinishedQuestEntity> selectList(Integer playerId);

    /**
     * 插入数据
     * @param entity 数据
     */
    @Override
    void insert(FinishedQuestEntity entity);
}
package org.yunai.swjg.server.module.item.operation.use;

import org.yunai.swjg.server.module.item.vo.Item;
import org.yunai.swjg.server.module.player.vo.Player;

/**
 * 使用道具的策略接口<br />
 * 实现类应该是无状态的，可以被公用的
 * User: yunai
 * Date: 13-6-2
 * Time: 上午11:55
 */
public interface ItemUseOperation {

    /**
     * 判断使用操作是否合适
     *
     * @param player 玩家信息
     * @param item 道具
     * @param targetId 目标编号
     * @return 操作是否合适
     */
    boolean isSuitable(Player player, Item item, int targetId);

    /**
     * 使用，并返回使用结果
     *
     * @param player 玩家信息
     * @param item 道具
     * @param targetId 目标编号
     * @param param 拓展参数
     * @return 使用结果
     */
    boolean use(Player player, Item item, int targetId, int param);
}
package org.yunai.swjg.server.module.activity.msg;

import org.yunai.swjg.server.module.activity.ActivityDef;
import org.yunai.swjg.server.module.activity.ActivityService;
import org.yunai.yfserver.message.sys.SysInternalMessage;
import org.yunai.yfserver.spring.BeanManager;

/**
 * 活动改变状态消息<br />
 * 系统内部调用消息
 * User: yunai
 * Date: 13-5-21
 * Time: 上午11:38
 */
public class SysActivityChangeStatusMessage
        extends SysInternalMessage {

    private static ActivityService activityService;
    static {
        activityService = BeanManager.getBean(ActivityService.class);
    }

    /**
     * 活动编号
     */
    private final Integer actId;
    /**
     * 目标活动状态
     */
    private final ActivityDef.Status status;

    public SysActivityChangeStatusMessage(Integer actId, ActivityDef.Status status) {
        this.actId = actId;
        this.status = status;
    }

    @Override
    public void execute() {
        activityService.changeStatus(actId, status);
    }

    @Override
    public short getCode() {
        return 0;
    }
}

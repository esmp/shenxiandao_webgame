package org.yunai.swjg.server.module.battle.command;

import org.springframework.stereotype.Service;
import org.yunai.swjg.server.core.constants.SysMessageConstants;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.activity.BossActivity;
import org.yunai.swjg.server.module.battle.operation.PVMBossBattleOperation;
import org.yunai.swjg.server.module.battle.operation.PVMMonsterBattleOperation;
import org.yunai.swjg.server.module.monster.vo.VisibleMonster;
import org.yunai.swjg.server.module.rep.RepScene;
import org.yunai.swjg.server.rpc.message.S_C.S_C_SysMessageReq;
import org.yunai.yfserver.async.IIoOperationService;

import javax.annotation.Resource;

/**
 * 战斗Service
 * User: yunai
 * Date: 13-5-17
 * Time: 下午9:08
 */
@Service
public class BattleService {

    @Resource
    private IIoOperationService ioOperationService;

    public void battleRepMonster(Online online, Integer monsterId) {
        if (!online.isInRepScene()) {
            return;
        }
        RepScene rep = (RepScene) online.getScene();
        VisibleMonster visibleMonster = rep.getMonster(monsterId);
        if (!checkMonsterStatus(online, visibleMonster)) {
            return;
        }
        ioOperationService.syncExecute(new PVMMonsterBattleOperation(online, visibleMonster));
    }

    public void battleBossMonster(Online online, BossActivity bossActivity, VisibleMonster monster) {
        if (!online.isInActivityRepScene()) {
            return;
        }
        if (!checkMonsterStatus(online, monster)) {
            return;
        }
        ioOperationService.syncExecute(new PVMBossBattleOperation(online, monster, bossActivity));
    }

    private boolean checkMonsterStatus(Online online, VisibleMonster visibleMonster) {
        if (visibleMonster == null) {
            online.write(new S_C_SysMessageReq(SysMessageConstants.MONSTER_NOT_FOUND));
            return false;
        }
        if (!visibleMonster.isAlive()) {
            online.write(new S_C_SysMessageReq(SysMessageConstants.MONSTER_DEAD));
            return false;
        }
        return true;
    }
}

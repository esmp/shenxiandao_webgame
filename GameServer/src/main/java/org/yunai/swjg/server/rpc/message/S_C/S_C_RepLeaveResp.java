package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【20404】: 离开副本请求
 */
public class S_C_RepLeaveResp extends GameMessage {
    public static final short CODE = 20404;

    /**
     * 原来场景ID
     */
    private Integer sceneId;
    /**
     * 原来场景坐标x
     */
    private Short sceneX;
    /**
     * 原来场景坐标y
     */
    private Short sceneY;

    public S_C_RepLeaveResp() {
    }

    public S_C_RepLeaveResp(Integer sceneId, Short sceneX, Short sceneY) {
        this.sceneId = sceneId;
        this.sceneX = sceneX;
        this.sceneY = sceneY;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Integer getSceneId() {
		return sceneId;
	}

	public void setSceneId(Integer sceneId) {
		this.sceneId = sceneId;
	}
	public Short getSceneX() {
		return sceneX;
	}

	public void setSceneX(Short sceneX) {
		this.sceneX = sceneX;
	}
	public Short getSceneY() {
		return sceneY;
	}

	public void setSceneY(Short sceneY) {
		this.sceneY = sceneY;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_RepLeaveResp struct = new S_C_RepLeaveResp();
            struct.setSceneId(byteArray.getInt());
            struct.setSceneX(byteArray.getShort());
            struct.setSceneY(byteArray.getShort());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_RepLeaveResp struct = (S_C_RepLeaveResp) message;
            ByteArray byteArray = ByteArray.createNull(8);
            byteArray.create();
            byteArray.putInt(struct.getSceneId());
            byteArray.putShort(struct.getSceneX());
            byteArray.putShort(struct.getSceneY());
            return byteArray;
        }
    }
}
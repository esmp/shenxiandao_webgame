package org.yunai.swjg.server.module.arena.command;

import org.springframework.stereotype.Controller;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.arena.ArenaService;
import org.yunai.swjg.server.rpc.message.C_S.C_S_ArenaDetailReq;

/**
 * 竞技场详细消息命令
 * User: yunai
 * Date: 13-5-28
 * Time: 下午11:50
 */
@Controller
public class ArenaDetailCommand extends GameMessageCommand<C_S_ArenaDetailReq> {

    private ArenaService arenaService;

    @Override
    protected void execute(Online online, C_S_ArenaDetailReq msg) {
        arenaService.detail(online);
    }
}

package org.yunai.swjg.server.module.item.template;

import org.jumpmind.symmetric.csv.CsvReader;
import org.yunai.swjg.server.module.player.VocationEnum;
import org.yunai.yfserver.util.CsvUtil;
import org.yunai.yfserver.util.StringUtils;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * 装备道具模版
 * User: yunai
 * Date: 13-4-7
 * Time: 下午9:53
 */
public class EquipmentItemTemplate extends ItemTemplate {

    /**
     * 职业
     */
    private Set<VocationEnum> vocations;
    /**
     * 基础普通攻击
     */
    private int baseNormalAttack;
    /**
     * 成长普通攻击
     */
    private int growNormalAttack;
    /**
     * 基础角色攻击
     */
    private int baseStuntAttack;
    /**
     * 成长角色攻击
     */
    private int growStuntAttack;
    /**
     * 基础法术攻击
     */
    private int baseMagicAttack;
    /**
     * 成长法术攻击
     */
    private int growMagicAttack;
    /**
     * 基础普通防御
     */
    private int baseNormalDefense;
    /**
     * 成长普通防御
     */
    private int growNormalDefense;
    /**
     * 基础角色防御
     */
    private int baseStuntDefense;
    /**
     * 成长角色防御
     */
    private int growStuntDefense;
    /**
     * 基础HP
     */
    private int baseHp;
    /**
     * 成长HP
     */
    private int growHp;
    /**
     * 基础先攻
     */
    private int baseXianGong;
    /**
     * 成长先攻
     */
    private int growXianGong;
    /**
     * 合成时，缺少所需元宝
     */
    private int gold;

    public Set<VocationEnum> getVocations() {
        return vocations;
    }

    public int getBaseNormalAttack() {
        return baseNormalAttack;
    }

    public int getGrowNormalAttack() {
        return growNormalAttack;
    }

    public int getBaseStuntAttack() {
        return baseStuntAttack;
    }

    public int getGrowStuntAttack() {
        return growStuntAttack;
    }

    public int getBaseMagicAttack() {
        return baseMagicAttack;
    }

    public int getGrowMagicAttack() {
        return growMagicAttack;
    }

    public int getBaseNormalDefense() {
        return baseNormalDefense;
    }

    public int getGrowNormalDefense() {
        return growNormalDefense;
    }

    public int getBaseStuntDefense() {
        return baseStuntDefense;
    }

    public int getGrowStuntDefense() {
        return growStuntDefense;
    }

    public int getBaseHp() {
        return baseHp;
    }

    public int getGrowHp() {
        return growHp;
    }

    public int getBaseXianGong() {
        return baseXianGong;
    }

    public int getGrowXianGong() {
        return growXianGong;
    }

    public int getGold() {
        return gold;
    }

    // ==================== 非set/get方法 ====================
    @Override
    protected void genTemplate(CsvReader reader) throws IOException {
        short[] vocationObjs = StringUtils.splitShort(CsvUtil.getString(reader, "vocations", ""), ",");
        vocations = new HashSet<>(vocationObjs.length);
        for (short vocation : vocationObjs) {
            vocations.add(VocationEnum.valueOf(vocation));
        }
        baseNormalAttack = CsvUtil.getInt(reader, "baseNormalAttack", 0);
        growNormalAttack = CsvUtil.getInt(reader, "growNormalAttack", 0);
        baseStuntAttack = CsvUtil.getInt(reader, "baseStuntAttack", 0);
        growStuntAttack = CsvUtil.getInt(reader, "growStuntAttack", 0);
        baseMagicAttack = CsvUtil.getInt(reader, "baseMagicAttack", 0);
        growMagicAttack = CsvUtil.getInt(reader, "growMagicAttack", 0);
        baseNormalDefense = CsvUtil.getInt(reader, "baseNormalDefense", 0);
        growNormalDefense = CsvUtil.getInt(reader, "growNormalDefense", 0);
        baseStuntDefense = CsvUtil.getInt(reader, "baseStuntDefense", 0);
        growStuntDefense = CsvUtil.getInt(reader, "growStuntDefense", 0);
        baseHp = CsvUtil.getInt(reader, "baseHp", 0);
        growHp = CsvUtil.getInt(reader, "growHp", 0);
        baseXianGong = CsvUtil.getInt(reader, "baseXianGong", 0);
        growXianGong = CsvUtil.getInt(reader, "growXianGong", 0);
        gold = CsvUtil.getInt(reader, "gold", 0);
    }

    @Override
    protected void check() {

    }
}
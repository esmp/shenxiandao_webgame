package org.yunai.swjg.server.core;

/**
 * 游戏服务器运行时数据
 * User: yunai
 * Date: 13-3-27
 * Time: 下午2:43
 */
public class GameServerRuntime {
    /**
     * 服务器是否开放，默认为false
     */
    private static volatile boolean open = false;
    /**
     * 读流量是否进行控制，默认为true
     */
    private static volatile boolean readTrafficControl = true;
    /**
     * 写流量是否进行控制，默认为true
     * TODO 看情况是否实现
     */
    private static volatile boolean writeTrafficControl = true;
    /**
     * 玩家的session里写队列的最大字节数，超出此值将其连接断开
     * TODO 看情况是否实现
     */
    private static final int MAX_WRITE_BYTES_IN_QUEUE = 300 * 1024;

    private GameServerRuntime() {
    }

    /**
     * @return 服务器是否开放
     */
    public static boolean isOpen() {
        return open;
    }

    /**
     * 设置服务器打开
     */
    public static void setOpenOn() {
        open = true;
    }

    /**
     * 关闭服务器
     */
    public static void shutdown() {
        open = false;
    }

    /**
     * @return 读流量是否进行控制
     */
    public static boolean isReadTrafficControl() {
        return readTrafficControl;
    }

    /**
     * 设置读流量是否进行控制
     *
     * @param readTrafficControl 读流量是否进行控制
     */
    public static void setReadTrafficControl(boolean readTrafficControl) {
        GameServerRuntime.readTrafficControl = readTrafficControl;
    }
}

package org.yunai.swjg.server.module.player.template;

import org.jumpmind.symmetric.csv.CsvReader;
import org.yunai.swjg.server.module.player.VocationEnum;
import org.yunai.swjg.server.module.skill.template.SkillTemplate;
import org.yunai.yfserver.util.CsvUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 职业模版
 * User: yunai
 * Date: 13-5-3
 * Time: 下午9:09
 */
public class VocationTemplate {

    /**
     * 职业编号
     */
    private VocationEnum vocation;
    /**
     * 基础武力(该属性只对玩家玩家创建的角色有效)，另外最终计算属性时，会减去该属性，实际该属性对最终面板没影响。
     */
    private int baseStrength;
    /**
     * 基础绝技(该属性只对玩家玩家创建的角色有效)，另外最终计算属性时，会减去该属性，实际该属性对最终面板没影响。
     */
    private int baseStunt;
    /**
     * 基础法术(该属性只对玩家玩家创建的角色有效)，另外最终计算属性时，会减去该属性，实际该属性对最终面板没影响。
     */
    private int baseMagic;
    /**
     * 基础HP(该属性只对玩家玩家创建的角色有效)。
     */
    private int baseHp;
    /**
     * 基础普通攻击
     */
    private int baseStrengthAttack;
    /**
     * 基础绝技攻击
     */
    private int baseStuntAttack;
    /**
     * 基础法术攻击
     */
    private int baseMagicAttack;
    /**
     * 基础普通防御
     */
    private int baseStrengthDefense;
    /**
     * 基础绝技防御
     */
    private int baseStuntDefense;
    /**
     * 基础法术防御
     */
    private int baseMagicDefense;
    /**
     * 成长HP
     */
    private int growHp;
    /**
     * 成长普通攻击
     */
    private int growStrengthAttack;
    /**
     * 成长绝技攻击
     */
    private int growStuntAttack;
    /**
     * 成长法术攻击
     */
    private int growMagicAttack;
    /**
     * 成长普通防御
     */
    private int growStrengthDefense;
    /**
     * 成长绝技防御
     */
    private int growStuntDefense;
    /**
     * 成长法术防御
     */
    private int growMagicDefense;
    /**
     * 基础暴击，万分比
     */
    private int baseCrit;
    /**
     * 基础命中，万分比
     */
    private int baseHit;
    /**
     * 基础破击，万分比
     */
    private int basePoJi;
    /**
     * 基础必杀，万分比
     */
    private int baseBiSha;
    /**
     * 基础韧性，万分比
     */
    private int baseToughness;
    /**
     * 基础闪避，万分比
     */
    private int baseDodge;
    /**
     * 基础格挡，万分比
     */
    private int baseBlock;
    /**
     * 技能模版
     */
    private SkillTemplate skill;
    /**
     * 玩家创建时，赠送的技能
     */
    private SkillTemplate playerCreateSkill;

    public VocationEnum getVocation() {
        return vocation;
    }

    public int getBaseStrength() {
        return baseStrength;
    }

    public int getBaseStunt() {
        return baseStunt;
    }

    public int getBaseMagic() {
        return baseMagic;
    }

    public int getBaseHp() {
        return baseHp;
    }

    public int getBaseStrengthAttack() {
        return baseStrengthAttack;
    }

    public int getBaseStuntAttack() {
        return baseStuntAttack;
    }

    public int getBaseMagicAttack() {
        return baseMagicAttack;
    }

    public int getBaseStrengthDefense() {
        return baseStrengthDefense;
    }

    public int getBaseStuntDefense() {
        return baseStuntDefense;
    }

    public int getBaseMagicDefense() {
        return baseMagicDefense;
    }

    public int getGrowHp() {
        return growHp;
    }

    public int getGrowStrengthAttack() {
        return growStrengthAttack;
    }

    public int getGrowStuntAttack() {
        return growStuntAttack;
    }

    public int getGrowMagicAttack() {
        return growMagicAttack;
    }

    public int getGrowStrengthDefense() {
        return growStrengthDefense;
    }

    public int getGrowStuntDefense() {
        return growStuntDefense;
    }

    public int getGrowMagicDefense() {
        return growMagicDefense;
    }

    public int getBaseCrit() {
        return baseCrit;
    }

    public int getBaseHit() {
        return baseHit;
    }

    public int getBasePoJi() {
        return basePoJi;
    }

    public int getBaseBiSha() {
        return baseBiSha;
    }

    public int getBaseToughness() {
        return baseToughness;
    }

    public int getBaseDodge() {
        return baseDodge;
    }

    public int getBaseBlock() {
        return baseBlock;
    }

    public SkillTemplate getSkill() {
        return skill;
    }

    public void setSkill(SkillTemplate skill) {
        this.skill = skill;
    }

    public SkillTemplate getPlayerCreateSkill() {
        return playerCreateSkill;
    }

    public void setPlayerCreateSkill(SkillTemplate playerCreateSkill) {
        this.playerCreateSkill = playerCreateSkill;
    }

    // ==================== 非set/get方法 ====================
    private static Map<VocationEnum, VocationTemplate> templates;

    public static VocationTemplate get(VocationEnum vocation) {
        return templates.get(vocation);
    }

    public static VocationTemplate get(short vocation) {
        return templates.get(VocationEnum.valueOf(vocation));
    }

    public static void load() {
        CsvReader reader = null;
        // 职业公用模版
        templates = new HashMap<>();
        try {
            reader = CsvUtil.createReader("csv/player/vocation_template.csv");
            reader.readHeaders();
            while (reader.readRecord()) {
                VocationTemplate template = genVocationTemplate(reader);
                templates.put(template.getVocation(), template);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        // TODO check
    }

    private static VocationTemplate genVocationTemplate(CsvReader reader) throws IOException {
        VocationTemplate template = new VocationTemplate();
        template.vocation = VocationEnum.valueOf(CsvUtil.getShort(reader, "vocation", (short) 0));
        template.baseStrength = CsvUtil.getInt(reader, "baseStrength", 0);
        template.baseStunt = CsvUtil.getInt(reader, "baseStunt", 0);
        template.baseMagic = CsvUtil.getInt(reader, "baseMagic", 0);
        template.baseHp = CsvUtil.getInt(reader, "baseHp", 0);
        template.baseStrengthAttack = CsvUtil.getInt(reader, "baseStrengthAttack", 0);
        template.baseStuntAttack = CsvUtil.getInt(reader, "baseStrengthAttack", 0);
        template.baseMagicAttack = CsvUtil.getInt(reader, "baseMagicAttack", 0);
        template.baseStrengthDefense = CsvUtil.getInt(reader, "baseStrengthDefense", 0);
        template.baseStuntDefense = CsvUtil.getInt(reader, "baseStrengthDefense", 0);
        template.baseMagicDefense = CsvUtil.getInt(reader, "baseMagicDefense", 0);
        template.growHp = CsvUtil.getInt(reader, "growHp", 0);
        template.growStrengthAttack = CsvUtil.getInt(reader, "growStrengthAttack", 0);
        template.growStuntAttack = CsvUtil.getInt(reader, "growStrengthAttack", 0);
        template.growMagicAttack = CsvUtil.getInt(reader, "growMagicAttack", 0);
        template.growStrengthDefense = CsvUtil.getInt(reader, "growStrengthDefense", 0);
        template.growStuntDefense = CsvUtil.getInt(reader, "growStrengthDefense", 0);
        template.growMagicDefense = CsvUtil.getInt(reader, "growMagicDefense", 0);
        template.baseCrit = CsvUtil.getInt(reader, "baseCrit", 0);
        template.baseHit = CsvUtil.getInt(reader, "baseHit", 0);
        template.basePoJi = CsvUtil.getInt(reader, "basePoJi", 0);
        template.baseBiSha = CsvUtil.getInt(reader, "baseBiSha", 0);
        template.baseToughness = CsvUtil.getInt(reader, "baseToughness", 0);
        template.baseDodge = CsvUtil.getInt(reader, "baseDodge", 0);
        template.baseBlock = CsvUtil.getInt(reader, "baseBlock", 0);
        template.skill = SkillTemplate.getItemTemplate(CsvUtil.getInt(reader, "skill", 0));
        template.playerCreateSkill = SkillTemplate.getItemTemplate(CsvUtil.getInt(reader, "playerCreateSkill", 0));
        return template;
    }
}

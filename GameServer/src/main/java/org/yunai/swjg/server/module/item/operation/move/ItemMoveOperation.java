package org.yunai.swjg.server.module.item.operation.move;

import org.yunai.swjg.server.module.item.container.CommonBag;
import org.yunai.swjg.server.module.item.vo.Item;
import org.yunai.swjg.server.module.player.vo.Player;

/**
 * 移动道具操作接口
 * User: yunai
 * Date: 13-6-6
 * Time: 下午7:38
 */
public interface ItemMoveOperation {

    /**
     * 判断移动操作是否合适
     *
     * @param player  玩家信息
     * @param item    道具
     * @param toBag   目标背包
     * @param toIndex 目标背包位置
     * @return 操作是否合适
     */
    boolean isSuitable(Player player, Item item, CommonBag toBag, int toIndex);

    /**
     * 移动，并返回移动结果
     *
     * @param player  玩家信息
     * @param item    道具
     * @param toBag   目标背包
     * @param toIndex 目标背包位置
     * @return 移动结果
     */
    boolean move(Player player, Item item, CommonBag toBag, int toIndex);
}
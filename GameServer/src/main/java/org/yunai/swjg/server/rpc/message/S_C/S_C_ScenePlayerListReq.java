package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import java.util.List;
import org.yunai.swjg.server.rpc.struct.StPlayerInfo;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【20603】: 场景玩家列表请求
 */
public class S_C_ScenePlayerListReq extends GameMessage {
    public static final short CODE = 20603;

    /**
     * 玩家信息结构体列表
     */
    private List<StPlayerInfo> players;

    public S_C_ScenePlayerListReq() {
    }

    public S_C_ScenePlayerListReq(List<StPlayerInfo> players) {
        this.players = players;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public List<StPlayerInfo> getPlayers() {
		return players;
	}

	public void setPlayers(List<StPlayerInfo> players) {
		this.players = players;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_ScenePlayerListReq struct = new S_C_ScenePlayerListReq();
		struct.setPlayers(getMessageList(StPlayerInfo.Decoder.getInstance(), byteArray, StPlayerInfo.class));
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_ScenePlayerListReq struct = (S_C_ScenePlayerListReq) message;
            ByteArray byteArray = ByteArray.createNull(0);
            byte[][] playersBytes = convertMessageList(byteArray, StPlayerInfo.Encoder.getInstance(), struct.getPlayers());
            byteArray.create();
            putMessageList(byteArray, playersBytes);
            return byteArray;
        }
    }
}
package org.yunai.swjg.server.module.quest.condition;

import org.yunai.swjg.server.module.quest.QuestDef;
import org.yunai.yfserver.util.Assert;
import org.yunai.yfserver.util.StringUtils;

/**
 * 任务条件简单工厂
 * User: yunai
 * Date: 13-5-9
 * Time: 上午1:28
 */
public class QuestConditionFactory {

    /**
     * 根据字符串生成任务条件对象
     *
     * @param str 字符串条件
     * @return 任务条件对象
     */
    public static IQuestCondition create(String str) {
        // 验证
        Assert.notNull(str, "str参数不能为空");
        int[] strs = StringUtils.splitInt(str, "\\|");
        Assert.isTrue(strs.length == 3, "str参数长度错误");
        // 产生条件
        QuestDef.Condition condition = QuestDef.Condition.valueOf(strs[0]);
        switch (condition) {
            case KILL: return new QuestKillCondition(strs[1], strs[2]);
            case ITEM: return new QuestItemCondition(strs[1], strs[2]);
            default: throw new IllegalStateException("不支持的任务类型");
        }
    }
}

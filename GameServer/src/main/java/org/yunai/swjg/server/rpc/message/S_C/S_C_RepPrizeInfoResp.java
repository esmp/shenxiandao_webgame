package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【20406】: 副本奖励信息
 */
public class S_C_RepPrizeInfoResp extends GameMessage {
    public static final short CODE = 20406;

    /**
     * 奖励铜钱
     */
    private Integer coin;
    /**
     * 奖励经验
     */
    private Integer exp;
    /**
     * 评分
     */
    private Byte score;
    /**
     * 奖励阅历
     */
    private Integer soul;
    /**
     * 奖励元宝
     */
    private Integer gold;

    public S_C_RepPrizeInfoResp() {
    }

    public S_C_RepPrizeInfoResp(Integer coin, Integer exp, Byte score, Integer soul, Integer gold) {
        this.coin = coin;
        this.exp = exp;
        this.score = score;
        this.soul = soul;
        this.gold = gold;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Integer getCoin() {
		return coin;
	}

	public void setCoin(Integer coin) {
		this.coin = coin;
	}
	public Integer getExp() {
		return exp;
	}

	public void setExp(Integer exp) {
		this.exp = exp;
	}
	public Byte getScore() {
		return score;
	}

	public void setScore(Byte score) {
		this.score = score;
	}
	public Integer getSoul() {
		return soul;
	}

	public void setSoul(Integer soul) {
		this.soul = soul;
	}
	public Integer getGold() {
		return gold;
	}

	public void setGold(Integer gold) {
		this.gold = gold;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_RepPrizeInfoResp struct = new S_C_RepPrizeInfoResp();
            struct.setCoin(byteArray.getInt());
            struct.setExp(byteArray.getInt());
            struct.setScore(byteArray.getByte());
            struct.setSoul(byteArray.getInt());
            struct.setGold(byteArray.getInt());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_RepPrizeInfoResp struct = (S_C_RepPrizeInfoResp) message;
            ByteArray byteArray = ByteArray.createNull(17);
            byteArray.create();
            byteArray.putInt(struct.getCoin());
            byteArray.putInt(struct.getExp());
            byteArray.putByte(struct.getScore());
            byteArray.putInt(struct.getSoul());
            byteArray.putInt(struct.getGold());
            return byteArray;
        }
    }
}
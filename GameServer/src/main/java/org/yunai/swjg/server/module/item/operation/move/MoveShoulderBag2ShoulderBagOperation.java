package org.yunai.swjg.server.module.item.operation.move;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.core.constants.SysMessageConstants;
import org.yunai.swjg.server.module.item.Inventory;
import org.yunai.swjg.server.module.item.container.Bag;
import org.yunai.swjg.server.module.item.container.CommonBag;
import org.yunai.swjg.server.module.item.vo.Item;
import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.swjg.server.rpc.message.S_C.S_C_SysMessageReq;

import java.util.ArrayList;
import java.util.List;

/**
 * 主背包 -> 仓库背包
 * 主背包 -> 主背包
 * 仓库背包 -> 主背包
 * 仓库背包 -> 仓库背包
 * User: yunai
 * Date: 13-6-7
 * Time: 上午10:06
 */
@Component
public class MoveShoulderBag2ShoulderBagOperation extends AbstractItemMoveOperation {

    @Override
    public boolean isSuitable(Player player, Item item, CommonBag toBag, int toIndex) {
        return (item.getBagType() == Bag.BagType.PRIM || item.getBagType() == Bag.BagType.DEPOT)
                && (toBag.getBagType() == Bag.BagType.DEPOT || toBag.getBagType() == Bag.BagType.PRIM);
    }

    @Override
    protected boolean canMoveImpl(Player player, Item item, CommonBag toBag, int toIndex) {
        return true;
    }

    /**
     * 移动，会有如下情况：
     * <pre>
     *     1. 目标位置为空，则直接移动
     *     2. 目标位置不为空
     *     >>2.1 若可叠加（道具可叠加+道具为相同模版），则进行叠加操作
     *     >>2.2 若不可叠加，进行交换
     * </pre>
     *
     * @param player  玩家信息
     * @param item    道具
     * @param toBag   目标背包
     * @param toIndex 目标背包位置
     * @return 移动结果
     */
    @Override
    protected boolean moveImpl(Player player, Item item, CommonBag toBag, int toIndex) {
        Item toItem = toBag.getByIndex(toIndex);
        List<Item> changedItems = new ArrayList<>(2);
        Inventory inventory = player.getInventory();
        if (Item.isEmpty(toItem)) { // 目标位置为空
            Item msgDelItem = Item.buildRemoveItem(item);
            if (!item.getBag().move(item, toBag, toIndex)) {
                item.getPlayer().message(new S_C_SysMessageReq(SysMessageConstants.ITEM_MOVE_FAIL));
                return false;
            }
            changedItems.add(item);
            changedItems.add(msgDelItem);
        } else {
            if (item.canOverlap() && item.getTemplateId() == toItem.getTemplateId()) { // 目标位置可叠加
                int totalOverlap = item.getOverlap() + toItem.getOverlap();
                if (totalOverlap >= item.getMaxOverlap()) {
                    toItem.changeOverlap(item.getMaxOverlap());
                    item.changeOverlap(totalOverlap - item.getMaxOverlap());
                } else {
                    toItem.changeOverlap(totalOverlap);
                    item.changeOverlap(0);
                }
                changedItems.add(item);
                changedItems.add(toItem);
            } else { // 目标位置不可叠加，进行交换
                inventory.swap(item, toItem);
                changedItems.add(item);
                changedItems.add(toItem);
            }
        }
        inventory.sendModifyMessage(changedItems);
        return true;
    }
}
package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【20609】: 场景中玩家坐标变化响应
 */
public class S_C_ScenePlayerChangePosResp extends GameMessage {
    public static final short CODE = 20609;

    /**
     * 玩家编号
     */
    private Integer playerId;
    /**
     * 场景坐标x
     */
    private Short sceneX;
    /**
     * 场景坐标y
     */
    private Short sceneY;

    public S_C_ScenePlayerChangePosResp() {
    }

    public S_C_ScenePlayerChangePosResp(Integer playerId, Short sceneX, Short sceneY) {
        this.playerId = playerId;
        this.sceneX = sceneX;
        this.sceneY = sceneY;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Integer getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Integer playerId) {
		this.playerId = playerId;
	}
	public Short getSceneX() {
		return sceneX;
	}

	public void setSceneX(Short sceneX) {
		this.sceneX = sceneX;
	}
	public Short getSceneY() {
		return sceneY;
	}

	public void setSceneY(Short sceneY) {
		this.sceneY = sceneY;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_ScenePlayerChangePosResp struct = new S_C_ScenePlayerChangePosResp();
            struct.setPlayerId(byteArray.getInt());
            struct.setSceneX(byteArray.getShort());
            struct.setSceneY(byteArray.getShort());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_ScenePlayerChangePosResp struct = (S_C_ScenePlayerChangePosResp) message;
            ByteArray byteArray = ByteArray.createNull(8);
            byteArray.create();
            byteArray.putInt(struct.getPlayerId());
            byteArray.putShort(struct.getSceneX());
            byteArray.putShort(struct.getSceneY());
            return byteArray;
        }
    }
}
package org.yunai.swjg.server.module.rep.msg;

import org.slf4j.Logger;
import org.yunai.swjg.server.core.service.GameMessageProcessor;
import org.yunai.swjg.server.core.service.OnlineContextService;
import org.yunai.swjg.server.module.rep.RepSceneService;
import org.yunai.swjg.server.module.scene.core.msg.SysPlayerEnterSceneResult;
import org.yunai.yfserver.common.LoggerFactory;
import org.yunai.yfserver.message.sys.SysInternalMessage;
import org.yunai.yfserver.server.IMessageProcessor;
import org.yunai.yfserver.spring.BeanManager;

/**
 * 玩家进入副本<br />
 * 系统内部调用消息
 * User: yunai
 * Date: 13-5-13
 * Time: 下午3:23
 */
public class SysPlayerEnterRepSceneMessage
        extends SysInternalMessage {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerFactory.Logger.rep, SysPlayerEnterRepSceneMessage.class);

    private static RepSceneService repService;
    private static IMessageProcessor messageProcessor;
    private static OnlineContextService onlineContextService;
    static {
        repService = BeanManager.getBean(RepSceneService.class);
        messageProcessor = BeanManager.getBean(GameMessageProcessor.class);
        onlineContextService = BeanManager.getBean(OnlineContextService.class);
    }

    private final Integer playerId;
    /**
     * 场景编号
     */
    private final Integer sceneId;
    private final Integer line;

    public SysPlayerEnterRepSceneMessage(Integer playerId, Integer repId, Integer line) {
        this.playerId = playerId;
        this.sceneId = repId;
        this.line = line;
    }

    @Override
    public void execute() {
        boolean success = repService.handleEnterRep(playerId, sceneId, line);
        SysPlayerEnterSceneResult playerEnterSceneResult = new SysPlayerEnterSceneResult(playerId, success);
        messageProcessor.put(playerEnterSceneResult);
    }

    @Override
    public short getCode() {
        return 0;
    }
}
package org.yunai.swjg.server.module.formation;

import org.yunai.swjg.server.entity.FormationEntity;
import org.yunai.yfserver.persistence.orm.mybatis.Mapper;
import org.yunai.yfserver.persistence.orm.mybatis.SaveMapper;

/**
 * Formation数据访问接口
 * User: yunai
 * Date: 13-5-30
 * Time: 上午9:29
 */
public interface FormationMapper extends Mapper, SaveMapper<FormationEntity> {

    /**
     * 获得玩家阵形
     *
     * @param playerId 玩家编号
     * @return 阵形
     */
    FormationEntity select(int playerId);

    /**
     * 插入数据
     *
     * @param entity 数据
     */
    @Override
    void insert(FormationEntity entity);

    /**
     * 修改数据
     *
     * @param entity 数据
     */
    @Override
    void update(FormationEntity entity);
}

package org.yunai.swjg.server.module.player.command;

import org.springframework.stereotype.Component;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.player.operation.PlayerCreateOperation;
import org.yunai.swjg.server.rpc.message.C_S.C_S_PlayerCreateReq;
import org.yunai.yfserver.async.IIoOperationService;

import javax.annotation.Resource;

/**
 * 角色创建命令
 * User: yunai
 * Date: 13-3-30
 * Time: 下午4:26
 */
@Component
public class PlayerCreateCommand extends GameMessageCommand<C_S_PlayerCreateReq> {

    @Resource
    private IIoOperationService ioOperationService;

    @Override
    public void execute(Online online, C_S_PlayerCreateReq msg) {
        ioOperationService.asyncExecute(new PlayerCreateOperation(online, msg.getNickname(), msg.getVocation(), msg.getSex()));
    }
}

package org.yunai.yfserver.message.sys;

/**
 * 空的内部消息<br />
 * 用于一些地方有阻塞消息队列。
 *
 * User: yunai
 * Date: 13-4-28
 * Time: 下午4:23
 */
public class EmptySysInternalMessage extends SysInternalMessage {

    /**
     * 该方法不实现任务逻辑
     */
    @Override
    public void execute() {
    }

    @Override
    public short getCode() {
        return 0;
    }
}

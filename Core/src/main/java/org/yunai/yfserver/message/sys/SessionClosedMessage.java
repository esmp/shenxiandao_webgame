package org.yunai.yfserver.message.sys;

import org.yunai.yfserver.message.ISessionMessage;
import org.yunai.yfserver.message.MessageType;
import org.yunai.yfserver.session.ISession;

/**
 * Session关闭内部消息
 * User: yunai
 * Date: 13-3-26
 * Time: 下午11:58
 */
public class SessionClosedMessage<S extends ISession>
        extends SysInternalMessage
        implements ISessionMessage<S> {

    protected S session;

    @Override
    public short getCode() {
        return MessageType.SYS_SESSION_CLOSE;
    }

    @Override
    public void execute() {
    }

    @Override
    public S getSession() {
        return session;
    }

    @Override
    public void setSession(S session) {
        this.session = session;
    }
}

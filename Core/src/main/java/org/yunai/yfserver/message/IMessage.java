package org.yunai.yfserver.message;

/**
 * 消息接口
 * User: yunai
 * Date: 13-3-25
 * Time: 上午10:44
 */
public interface IMessage extends IStruct {

    /**
     * 获得消息编号
     * @return 消息编号
     */
    short getCode();

    /**
     * 执行消息
     */
    void execute();
    
}

package org.yunai.yfserver.message.sys;

import org.yunai.yfserver.message.IMessage;

/**
 * 系统内部产生的消息
 * User: yunai
 * Date: 13-3-26
 * Time: 下午11:17
 */
public abstract class SysInternalMessage implements IMessage {
}

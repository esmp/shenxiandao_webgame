package org.yunai.yfserver.time;

import org.yunai.yfserver.annotation.ThreadNotSafe;

/**
 * 系统时间服务器实现<br />
 * 该类主要是为了弥补{@link System#currentTimeMillis()}在高并发情况下的调用代价过高，<br />
 * 而较多情况下在某一时刻的缓存时间是可以满足业务需求的
 * User: yunai
 * Date: 13-4-27
 * Time: 下午5:10
 */
public class SystemTimeService implements TimeService {

    /**
     * 当前时间
     */
    private volatile long now = 0;
    /**
     * 是否使用缓存时间
     */
    private final boolean cacheTime;

    /**
     * 构造一个不缓存时间的时间管理器
     */
    public SystemTimeService() {
        this(false);
    }

    /**
     * 根据cacheTime来构造是否缓存时间的时间管理器
     *
     * @param cacheTime 是否缓存使用缓存时间
     */
    public SystemTimeService(boolean cacheTime) {
        this.cacheTime = cacheTime;
        if (this.cacheTime) {
            this.now = System.currentTimeMillis();
        }
    }

    /**
     * 更新当前时间<br />
     * 如果当前启动了缓存时间的策略，则需要在较短的间隔内调用该方法来更新被缓存的时间
     */
    @Override
    @ThreadNotSafe
    public void update() {
        if (cacheTime) {
            now = System.currentTimeMillis();
        }
    }

    /**
     * 获得当前的更新时间.<br />
     * 如果{@link #cacheTime}为true时，使用缓存时间
     * @return 当前的时间
     */
    @Override
    public long now() {
        if (cacheTime) {
            return now;
        }
        return System.currentTimeMillis();
    }

    /**
     * @param time 指定时间
     * @return 是否到达某个时间
     */
    @Override
    public boolean timeUp(long time) {
        return this.now() > time;
    }

    /**
     * @param time 指定时间
     * @return 是否到达某个时间
     */
    @Override
    public long getInterval(long time) {
        return time - this.now();
    }
}

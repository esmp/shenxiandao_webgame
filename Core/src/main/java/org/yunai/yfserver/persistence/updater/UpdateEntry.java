package org.yunai.yfserver.persistence.updater;

/**
 * 更新元素
 * User: yunai
 * Date: 13-4-9
 * Time: 下午3:37
 */
public class UpdateEntry<T> {
    /**
     * 操作枚举
     */
    public static enum Operation {
        /**
         * 更新操作
         */
        UPDATE,
        /**
         * 删除操作
         */
        DELETE
    }

    /**
     * 操作
     */
    private final Operation operation;
    /**
     * 待更新的业务对象
     */
    private final T obj;

    public UpdateEntry(Operation operation, T obj) {
        this.operation = operation;
        this.obj = obj;
    }

    public Operation getOperation() {
        return operation;
    }

    public T getObj() {
        return obj;
    }
}

package org.yunai.yfserver.persistence.orm;

import java.io.Serializable;

/**
 * 实体接口
 * User: yunai
 * Date: 13-4-6
 * Time: 上午11:19
 */
public interface Entity<ID extends Serializable> {

    ID getId();

    void setId(ID id);
}

package org.yunai.yfserver.persistence.updater;

/**
 * 数据更新器接口
 * User: yunai
 * Date: 13-4-9
 * Time: 下午3:31
 */
public interface DataUpdater<T> {

    /**
     * 添加修改数据
     * @param obj 更新数据
     * @return 添加结果
     */
    boolean addSave(final T obj);

    /**
     * 添加删除数据
     * @param obj 删除数据
     * @return 添加结果
     */
    boolean addDelete(final T obj);

    /**
     * 执行更新操作
     */
    void process();
}

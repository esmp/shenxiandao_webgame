package org.yunai.protobuf;

import org.yunai.protobuf.conf.Config;
import org.yunai.protobuf.conf.MessageConfig;
import org.yunai.protobuf.conf.ProjectConfig;
import org.yunai.protobuf.conf.StructConfig;
import org.yunai.protobuf.io.CodeGenerator;

import java.io.IOException;

//import org.yunai.protobuf.sb.message.S_C.S_C_OnlineResp;
//import org.yunai.protobuf.sb.struct.StPlayer;
//import org.yunai.protobuf.sb.struct.StProps;

/**
 * ProjectConfig测试类
 * User: yunai
 * Date: 13-3-12
 * Time: 下午11:09
 */
public class ProjectConfigTest {

    public static void main(String[] args) throws IOException {
        Config.filePath = "protobuf/ChatServer.xml";
//        Config.filePath = "protobuf/v1.xml";

        ProjectConfig.load();
        MessageConfig.load();
        StructConfig.load();

        CodeGenerator.execute();
    }

//    public static void main3(String[] args) {
//        StPlayer player = new StPlayer();
//        player.setNickname("骚球");
//        player.setSex((byte) 1);
//        player.setLevel((short) 70);
//        player.setExp(998L);
//
//        StPlayer.Encoder encoder = StPlayer.Encoder.getInstance();
//        ByteArray byteArray = encoder.encode(player);
//
//        byteArray.reset();
//        StPlayer decodeStPlayer = (StPlayer) StPlayer.Decoder.getInstance().decode(byteArray);
//        consolePlayer(decodeStPlayer);
//    }
//
//    public static void main5(String[] args) {
//        StPlayer player = new StPlayer();
//        player.setNickname("骚球");
//        player.setSex((byte) 1);
//        player.setLevel((short) 70);
//        player.setExp(998L);
//
//        List<StProps> propsList = new ArrayList<StProps>();
//        StProps props1 = new StProps();
//        props1.setId(1);
//        props1.setTotal(2);
//        propsList.add(props1);
//        StProps props2 = new StProps();
//        props2.setId(2);
//        props2.setTotal(3);
//        propsList.add(props2);
//        StProps props3 = new StProps();
//        props3.setId(3);
//        props3.setTotal(4);
//        propsList.add(props3);
//
//        S_C_OnlineResp online = new S_C_OnlineResp();
//        online.setInfo(player);
//        online.setFriendNum(10);
//        online.setProps(propsList);
//
//        S_C_OnlineResp.Encoder encoder = S_C_OnlineResp.Encoder.getInstance();
//        S_C_OnlineResp.Decoder decoder = S_C_OnlineResp.Decoder.getInstance();
//
//        ByteArray byteArray = encoder.encode(online);
//
//        byteArray.reset();
//        S_C_OnlineResp decodeOnline = (S_C_OnlineResp) decoder.decode(byteArray);
//        consoleOnline(decodeOnline);
//    }
//
//    private static void consolePlayer(StPlayer player) {
//        System.out.println(player.getNickname());
//        System.out.println(player.getSex());
//        System.out.println(player.getLevel());
//        System.out.println(player.getExp());
//    }
//
//    private static void consoleOnline(S_C_OnlineResp online) {
//        System.out.println(online.getFriendNum());
//        consolePlayer(online.getInfo());
//    }
}

// 2. 消息CODE